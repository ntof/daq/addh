/*
 * WriterFile.h
 *
 *  Created on: Aug 1, 2014
 *      Author: agiraud
 */

#ifndef WRITERFILE_H_
#define WRITERFILE_H_

#include <memory>
#include <thread>

#include <Queue.h>
#include <easylogging++.h>

#include "ADDHTypes.hpp"
#include "ADDHWriter.h"

namespace ntof {
namespace addh {

/**
 * @class WriterFile
 * @brief class to write the .run and .event files. The class get all the
 *        structures and organize them to write the index file
 **/
class WriterFile
{
public:
    /**
     * @brief Constructor of the class addh
     */
    explicit WriterFile(const std::shared_ptr<ADDH> &addh);

    /**
     * @brief Destructor of the class addh
     */
    virtual ~WriterFile();

    /**
     * @brief Add a command to the end of the list
     * @param command : the command to add at the list
     */
    void addCommand(const ADDHCommand &command);

    ErrorSignal errorSignal;
    WarningSignal warningSignal;

protected:
    /**
     * @brief Execute the action when a new command is received
     * @param command: the command name
     */
    void doAction(const ADDHCommand &command);

    /*
     * @brief Write the .event file
     */
    void writeFileEvent(const ADDHCommand &command);

    /*
     * @brief serialize addh data from clients to output
     */
    void dumpADDHData(const ClientsMap &clients, std::ofstream &out) const;

    /**
     * @brief send a service event for the given command
     * @param[in] cmd the command to notify about
     * @param[in] fileSize written file size
     *
     * @details this notification is used to synchonize EACS and ADDH services
     */
    void sendEvent(const ADDHCommand &cmd, size_t fileSize);

    /**
     * Thread procedure
     */
    void threadProc();

    std::unique_ptr<std::thread> m_thread;
    ntof::utils::Queue<ADDHCommand *> commands_; //!< The list of the
                                                 //!< commands received by
                                                 //!< the writer
    ntof::dim::DIMXMLService m_svcEvents;        //!< Writer events service
    const std::shared_ptr<ADDH> m_addh;          //!< source addh information
};
} // namespace addh
} // namespace ntof

#endif /* WRITERFILE_H_ */

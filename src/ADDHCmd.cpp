/*
 * ADDHCmd.cpp
 *
 *  Created on: Oct 23, 2014
 *      Author: agiraud
 */

#include "ADDHCmd.h"

#include <boost/filesystem.hpp>

#include "ADDHExtraService.hpp"
#include "Config.h"

namespace bfs = boost::filesystem;

namespace ntof {
namespace addh {
ADDHCmd::ADDHCmd() :
    DIMSuperCommand(
        Config::instance().getValue("commandServiceName", "ADDH/Command"))
{
    char hostname[_SC_HOST_NAME_MAX];
    gethostname(hostname, _SC_HOST_NAME_MAX);
    std::string hostnameString(hostname);
    this->m_filePathPrefix = Config::instance().getValue("outputDirPrefix", "");
}

void ADDHCmd::commandReceived(ntof::dim::DIMCmd &cmdData)
{
    // If the client sends non-valid data, the DIMSuperCommand sends
    // automatically a error to the client
    pugi::xml_node parent = cmdData.getData();

    if (parent)
    {
        std::string textValue = parent.attribute("name").as_string("");
        int32_t runNumber = parent.attribute("runNumber").as_int(-1);
        int32_t timingEvent = parent.attribute("timingEvent").as_int(-1);
        std::string filePath = parent.attribute("filePath").as_string("");
        std::string lsaCycle = parent.attribute("lsaCycle").as_string("");
        std::string user = parent.attribute("user").as_string("");

        std::transform(textValue.begin(), textValue.end(), textValue.begin(),
                       ::tolower);

        // Manage Extra command.
        if (textValue == "extra")
        {
            pugi::xml_node extraNode = parent.child("extra");
            if (ADDHExtraService::instance().executeCommand(extraNode))
            {
                setOk(cmdData.getKey(), std::string("OK!"));
            }
            else
            {
                setError(cmdData.getKey(), CMD_ERR,
                         "Error executing extra command.");
            }
            return;
        }

        ADDHCommand command;
        command.command = textValue;
        command.runNumber = runNumber;
        command.timingEvent = timingEvent;
        command.filePath = filePath;
        command.lsaCycle = lsaCycle;
        command.user = user;

        // Check if filePath is subpath of m_filePathPrefix
        if (isValidPath(filePath, m_filePathPrefix))
        {
            // Check if the command can be accepted
            LOG(INFO) << "Command " << textValue << " received.";
            if (textValue != "write" && textValue != "reset" &&
                textValue != "clear")
            {
                setError(cmdData.getKey(), CMD_UNKNOWN, "Command unknown.");
            }
            else
            {
                commandReceivedSignal(command);
                setOk(cmdData.getKey(), std::string("Done!"));
            }
        }
        else
        {
            setError(cmdData.getKey(), WRITE_ERR,
                     "Permission denied: invalid output path");
        }
    }
    else
    {
        LOG(INFO) << "The parent node of the command received is empty";
        setError(cmdData.getKey(), CMD_EMPTY, "Parent node is empty");
    }
}

bool ADDHCmd::isValidPath(const std::string &filePath,
                          const std::string &m_filePathPrefix)
{
    if (!m_filePathPrefix.empty())
    {
        bfs::path absPath = filePath;
        if (filePath[0] != '/')
        {
            // filePath is relative, convert to m_filePathPrefix/filePath form
            absPath = m_filePathPrefix + "/" + filePath;
        }
        try
        {
            absPath = bfs::canonical(absPath).string();
        }
        catch (std::exception const &e)
        {
            // No such file or directory
            return false;
        }
        return (absPath.string().compare(0, m_filePathPrefix.length(),
                                         m_filePathPrefix) == 0);
    }
    return true;
}

} // namespace addh
} // namespace ntof

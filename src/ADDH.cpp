/*
 * ADDH.cpp
 *
 *  Created on: Sep 24, 2014
 *      Author: agiraud
 */

#include "ADDH.h"

#include <easylogging++.h>

#include "ADDHClientHandler.h"
#include "ADDHException.h"
#include "ADDHExtraService.hpp"
#include "ADDHTypes.hpp"
#include "Config.h"

#include <dic.hxx>

namespace ntof {
namespace addh {

void ADDH::initConfigurations()
{
    // Initialize a client for each element of the ADDH (from the configuration
    // file)
    handleGlobalConfig();
    handleExtraConfig();

    // Listen on updateServicesSignal in order to update extra config
    m_extraConn = ADDHExtraService::instance().updateServicesSignal.connect(
        [this]() { handleExtraConfig(); });
}

ADDH::~ADDH()
{
    m_extraConn.disconnect();
}

void ADDH::handleGlobalConfig()
{
    LOG(INFO) << "Creating ADDH Clients from global configuration";
    ClientsConfigList addhData = Config::instance().getAddhData();
    createClients(addhData, m_clients);
}

void ADDH::handleExtraConfig()
{
    LOG(INFO) << "Updating ADDH Clients from extra configuration";
    ClientsConfigList extraData = ADDHExtraService::instance().getExtraData();
    int32_t startingIndex = Config::instance().getAddhData().size() + 1;
    createClients(extraData, m_extraClients, startingIndex);
}

const ClientsMap ADDH::getClients() const
{
    const std::lock_guard<std::mutex> lock(m_mutex);
    return m_clients;
}

const ClientsMap ADDH::getExtraClients() const
{
    const std::lock_guard<std::mutex> lock(m_mutex);
    return m_extraClients;
}

void ADDH::createClients(const ClientsConfigList &configs,
                         ClientsMap &clients,
                         int32_t index) const
{
    clients.clear();
    for (const ADDHClientConfig &data : configs)
    {
        data.log();

        // Check if the client already exists
        ClientsMap::iterator clientsIterator;
        clientsIterator = clients.find(data.fromService);
        if (clientsIterator == clients.end())
        {
            // Initialization of the handler for the client
            std::shared_ptr<ADDHClientHandler> handler(
                std::make_shared<ADDHClientHandler>(index++, data));
            // Connect error and warning signals
            handler->errorSignal.connect(errorSignal);
            handler->warningSignal.connect(warningSignal);

            ADDHClient addhClient;

            if (data.type == DataSourceType::VALUE)
            {
                std::shared_ptr<DimInfo> client(std::make_shared<DimInfo>(
                    data.fromService.c_str(), ((void *) NULL), 0,
                    handler.get()));
                // client->itsHandler = handler.get();
                addhClient = {handler, nullptr, client};
            }
            else
            {
                // Initialization of the client
                std::shared_ptr<ntof::dim::DIMProxyClient> client(
                    std::make_shared<ntof::dim::DIMProxyClient>(
                        data.fromService));
                client->setHandler(handler.get());

                addhClient = {handler, client, nullptr};
            }
            // Add the client in the vector list
            clients.insert(std::pair<std::string, ADDHClient>(data.fromService,
                                                              addhClient));
        }
        else
        {
            if (data.type == DataSourceType::SINGLE_DATA)
            {
                // Add the field to the client list
                clientsIterator->second.client->addField(data.fromName,
                                                         data.destination);
            }
            else if (data.type == DataSourceType::ALL_DATA)
            {
                // Add all the fields to the client list
                clientsIterator->second.client->addAllFields();
            }
            // If there is a DataSourceType::Value duplicate, we just ignore it
        }
    }
}

} // namespace addh
} // namespace ntof

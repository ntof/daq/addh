/*
 * ADDHClient.h
 *
 *  Created on: Oct 20, 2014
 *      Author: agiraud
 */

#ifndef ADDHCLIENT_H_
#define ADDHCLIENT_H_

#include <iostream>
#include <memory>
#include <vector>

#include <DIMProxyClient.h>
#include <DaqTypes.h>

#include "ADDH.h"
#include "ADDHTypes.hpp"

#include <dic.hxx>

namespace ntof {
namespace addh {
class ADDH;

/**
 * @class ADDHClient
 * @brief Dim Client which is listening the DIM Service published by the
 * Producer. The class update automatically the BEAM parameters
 */
class ADDHClientHandler :
    public ntof::dim::DIMProxyClientHandler,
    public DimInfoHandler
{
public:
    typedef std::vector<std::shared_ptr<AdditionalDataValue>> FieldList;
    /**
     *  @brief Constructor of the class ADDHClient
     *  @param prefix: Prefix of resulting fields
     */
    explicit ADDHClientHandler(int32_t m_index, const ADDHClientConfig &config);

    /**
     *  @brief Destructor of the class ADDHClient
     */
    ~ADDHClientHandler() override;

    /**
     *  @brief Function called each time the basic dim service change
     */
    void infoHandler() override;

    /**
     *  @brief Function called each time the service change
     *  @param dataSet: New list of data
     *  @param client: Client responsible of this callback (can be shared)
     */
    void dataChanged(std::vector<ntof::dim::DIMData> &dataSet,
                     const ntof::dim::DIMProxyClient &client) override;

    /**
     *  @brief Called when an error occured on client (NO-LINK, NOT READY...)
     *  @param errMsg Error message
     *  @param client Client responsible for this callback (can be shared)
     */
    void errorReceived(const std::string &errMsg,
                       const ntof::dim::DIMProxyClient &client) override;

    /**
     *  @brief Getter of the data
     *  @return The data header
     */
    FieldList getADDHData();

    /**
     *  @brief Allow to know if the data has been acquired correctly or not
     *  @return Return true if an error occurred, else it returns false
     */
    bool isInError() const;

    /*
     * @brief Add an field to the list of the elements to keep
     * @param from: Name of the field in the DIM service
     * @param destination: Name of the field in the ADDH
     */
    void addField(const std::string &from, const std::string &destination);

    /**
     * @brief Signal the ADDH client to save all fields in the dataset
     */
    void addAllFields();

    ErrorSignal errorSignal;
    WarningSignal warningSignal;

protected:
    typedef std::map<std::string, std::string> FieldMap; //!< Map associating
                                                         //!< data field source
                                                         //!< and destination

    /**
     * @brief create an ADDH value from a DIM data
     * @param[in]  name name of the data value
     * @param[in]  type ADDH Type
     * @param[in]  data raw pointer to data
     * @param[in]  count number of data items
     * @return a vector with all ADDH values
     */
    template<typename DestType, typename SourceType>
    static FieldList createADDH(const std::string &name,
                                AdditionalDataValue::Type type,
                                const SourceType *data,
                                size_t rows,
                                size_t columns);

    /**
     * @brief check if the format published by the dim service is supported
     * Only array amd multi-array of the same type/size are supported
     * @param[out]  rows number of rows
     * @param[out]  column number of columns
     */
    bool isFormatSupported(size_t &rows, size_t &columns);

    /**
     * @brief create an ADDH value from a basic DIM data
     * @param[in]  name name of the data value where to publish the dim value
     * @return a vector with all ADDH values
     */
    FieldList makeValue(const std::string &name);

    /**
     * @brief create an ADDH value from a DIM data
     * @param[in]  name name of the data value
     * @param[in]  data DIM data
     * @return     a new ADDH value
     */
    static FieldList::value_type makeValue(const std::string &name,
                                           const ntof::dim::DIMData &data);

    std::mutex m_mutex;        //!< Mutex used to avoid concurrent access
    FieldList m_data;          //!< Data storage
    std::string m_service;     //!< source service where the client is connected
    std::string m_fieldPrefix; //!< Prefix of resulting field name
    std::string m_destination; //!< destination used for basic dim values
    bool m_inError;            //!< Status of the client
    FieldMap m_fieldList;      //!< List of the field to save
    int32_t m_index;           //!< Index used for warning code
};

} // namespace addh
} // namespace ntof
#endif /* ADDHCLIENT_H_ */

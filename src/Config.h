/*
 * Config.h
 *
 *  Created on: Jan 29, 2015
 *      Author: agiraud
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include <map>
#include <string>
#include <vector>

#include <ConfigMisc.h>
#include <Singleton.hpp>
#include <easylogging++.h>
#include <sys/types.h>

#include "ADDHTypes.hpp"

namespace ntof {
namespace addh {

/**
 * @class Config
 * @brief Class used to read the config file
 */
class Config :
    public ntof::utils::ConfigMisc,
    public ntof::utils::Singleton<Config>
{
public:
    static const std::string configFile;      //!< Path of the config file
    static const std::string extraConfigFile; //!< Path of the extra config file
    static const std::string configMiscFile; //!< Path of the global config file

    /**
     * @brief Destructor of the class
     */
    virtual ~Config() = default;

    /**
     * @brief load configuration from the given files
     * @param[in] file the configuration file to load
     *
     * @details this method will destroy any existing Config and instantiate
     * the mutex again
     */
    static Config &load(const std::string &file,
                        const std::string &extraFile,
                        const std::string &miscFile);

    /**
     * @brief retrieve a valued config parameter
     * @param[in]  name         config value param
     * @param[in]  defaultValue default value if parameter not set
     * @return the config value or default
     */
    const std::string &getValue(
        const std::string &name,
        const std::string &defaultValue = std::string()) const;

    /**
     * @brief check if the given parameter exist in config
     * @param[in]  name the name to check
     * @return true if such parameter is set in config
     */
    bool hasValue(const std::string &name) const;

    /**
     * @brief Get extra config file path
     * @return extra config file path
     */
    const std::string &getExtraConfigFile() const;

    /**
     * @brief Get the collection of the ADDH fields
     * @return The collection of the ADDH fields
     */
    inline const ClientsConfigList &getAddhData() const { return m_addhData; }

    /**
     * @brief Get the revision number of the ADDH
     * @return The revision number of the ADDH
     */
    int32_t getAddhVersion() const { return m_addhVersion; }

    /**
     * @brief Utility to read services from xml config and save to vector
     * @param[in]  node the xml node containing all the services
     * @param[out]  services the list of parsed services
     */
    static void loadServices(const pugi::xml_node &node,
                             ClientsConfigList &services);

protected:
    friend class ntof::utils::Singleton<Config>;

    explicit Config(const std::string &file = configFile,
                    const std::string &extraFile = extraConfigFile,
                    const std::string &miscFile = configMiscFile);

    std::string m_extraConfigFile;
    std::map<std::string, std::string> m_values; //! simple config values (<name
                                                 //! value="" />)
    int32_t m_addhVersion;                       //!< Version of the ADDH
    ClientsConfigList m_addhData; //!< Collection of the ADDH fields
};

} /* namespace addh */
} /* namespace ntof */

#endif /* CONFIG_H_ */

/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-04-08T00:19:48+02:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/
#ifndef ADDH_ADDHTYPES_HPP
#define ADDH_ADDHTYPES_HPP

#include <map>
#include <string>

#include <DIMProxyClient.h>
#include <easylogging++.h>

#include <dic.hxx>

namespace ntof {
namespace addh {

class ADDHClientHandler;

struct ADDHClient
{
    std::shared_ptr<ADDHClientHandler> client; //!< The handler of the DIM service
    std::shared_ptr<ntof::dim::DIMProxyClient> proxy; //!< The DIM client for
    //!< DIMData
    std::shared_ptr<DimInfo> value; //!< The DIM client for basic DimService
};

typedef std::map<std::string, ADDHClient> ClientsMap;

enum DataSourceType
{
    SINGLE_DATA, // A single DIMData inside a Dataset/Parameters Service
    ALL_DATA,    // All the DimData of a Dataset/Parameters Service
    VALUE,       // Basic Dim Service
};
const std::string &toString(DataSourceType type);

struct ADDHClientConfig
{
    DataSourceType type;
    std::string destination;
    std::string destinationPrefix;
    std::string fromService;
    std::string fromName;

    void log() const
    {
        LOG(INFO) << "ADDH Client Configuration:";
        LOG(INFO) << " -Type:               " << toString(this->type);
        LOG(INFO) << " -From service:       " << this->fromService;

        if (this->type == DataSourceType::SINGLE_DATA)
        {
            LOG(INFO) << " -Destination:        " << this->destination;
            LOG(INFO) << " -From name:          " << this->fromName;
        }
        if (this->type == DataSourceType::ALL_DATA)
        {
            LOG(INFO) << " -Destination prefix: " << this->destinationPrefix;
        }
        if (this->type == DataSourceType::VALUE)
        {
            LOG(INFO) << " -Destination:        " << this->destination;
        }
    }
};

typedef std::vector<ADDHClientConfig> ClientsConfigList;

struct ADDHCommand
{
    std::string command;
    int32_t timingEvent;
    int32_t runNumber;
    std::string filePath;
    std::string lsaCycle;
    std::string user;
};

/* states of the ADDH Writer */
enum ADDHState
{
    UNDEFINED = 0,
    STARTED,
};

/* Errors and warnings codes */
enum ErrorsCodes
{
    // EACS_ERR = 1,
    INTERNAL_ERR = 2,
    // STATE_ERR = 10,
    // NO_LINK = 11,
    // NO_DAQ = 12,
    // DIM_ERR = 13,
    // DB_ERR = 20,
    ACK_ERR = 30,
    // RESET_TRIGG_ERR = 31,
    CMD_UNKNOWN = 32,
    CMD_ERR = 33,
    CMD_EMPTY = 34,
    // HEADER_REJECTED = 40,
    WRITE_ERR = 50,
    // TRIGG_INVALID = 60,
};

typedef ntof::utils::signal<void(int32_t code, const std::string &message)>
    ErrorSignal;
typedef ntof::utils::signal<void(int32_t code, const std::string &message)>
    WarningSignal;

} /* namespace addh */
} /* namespace ntof */

#endif // ADDH_ADDHTYPES_HPP

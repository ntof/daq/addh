/*
 * Config.cpp
 *
 *  Created on: Jan 29, 2015
 *      Author: agiraud
 */

#include "Config.h"

#include <map>
#include <string>

#include <pugixml.hpp>

#include "ADDHException.h"
#include "ADDHTypes.hpp"

#include <Singleton.hxx>

template class ntof::utils::Singleton<ntof::addh::Config>;

namespace ntof {
namespace addh {

const std::string Config::configFile = "/etc/ntof/nTOF_ADDH.xml";
const std::string Config::extraConfigFile = "/etc/ntof/nTOF_ADDH_extra.xml";
const std::string Config::configMiscFile = "/etc/ntof/misc.xml";

Config::Config(const std::string &file,
               const std::string &extraFile,
               const std::string &miscFile) :
    m_extraConfigFile(extraFile), ConfigMisc(miscFile)
{
    pugi::xml_document doc;

    // Read the config file
    pugi::xml_parse_result result = doc.load_file(file.c_str());
    if (!result)
    {
        ADDH_THROW("Unable to read the configuration file : " + file, 0);
    }
    else
    {
        // Parse the config file
        pugi::xml_node addhNode = doc.child("ADDH");
        for (pugi::xml_node_iterator it = addhNode.begin();
             it != addhNode.end(); ++it)
        {
            if (it->type() != pugi::node_element || !it->name())
                continue;

            pugi::xml_attribute value = it->attribute("value");
            if (value.empty())
                continue;

            m_values[it->name()] = value.as_string();
        }

        m_addhVersion = addhNode.child("header").attribute("version").as_int(0);
        loadServices(addhNode.child("header"), m_addhData);
    }
}

const std::string &Config::getValue(const std::string &name,
                                    const std::string &defaultValue) const
{
    std::map<std::string, std::string>::const_iterator it = m_values.find(name);
    if (it == m_values.end())
        return defaultValue;
    else
        return it->second;
}

bool Config::hasValue(const std::string &name) const
{
    return m_values.count(name) != 0;
}

Config &Config::load(const std::string &file,
                     const std::string &extraFile,
                     const std::string &miscFile)
{
    ntof::utils::SingletonMutex lock;

    if (m_instance)
        delete m_instance;

    m_instance = new Config(file, extraFile, miscFile);
    return *m_instance;
}

void Config::loadServices(const pugi::xml_node &node,
                          ClientsConfigList &services)
{
    services.clear();
    if (!node)
        return;

    for (pugi::xml_node nodes = node.first_child(); nodes;
         nodes = nodes.next_sibling())
    {
        const std::string &nodeName = nodes.name();
        if (nodeName == "data" || nodeName == "value")
        {
            ADDHClientConfig data;
            data.destination = nodes.attribute("destination").as_string("");
            data.destinationPrefix =
                nodes.attribute("destinationPrefix").as_string("");
            data.fromService = nodes.attribute("fromService").as_string("");
            data.fromName = nodes.attribute("fromName").as_string("");

            // Data Source Type
            if (nodeName == "value")
            {
                data.type = DataSourceType::VALUE;
            }
            else if (data.fromName.empty())
            {
                data.type = DataSourceType::ALL_DATA;
            }
            else
            {
                data.type = DataSourceType::SINGLE_DATA;
            }
            services.push_back(data);
        }
    }
}
const std::string &Config::getExtraConfigFile() const
{
    return m_extraConfigFile;
}

} /* namespace addh */
} /* namespace ntof */

/*
 * ADDHClient.cpp
 *
 *  Created on: Oct 20, 2014
 *      Author: agiraud
 */

#include "ADDHClientHandler.h"

#include <cmath>
#include <sstream>
#include <utility>

#include <boost/algorithm/string.hpp>

#include <DIMData.h>
#include <DIMUtils.hpp>
#include <easylogging++.h>

#include "ADDHException.h"
#include "Config.h"

namespace ntof {
namespace addh {

ADDHClientHandler::ADDHClientHandler(int32_t index,
                                     const ADDHClientConfig &config) :
    m_inError(false), m_index(index)
{
    m_service = config.fromService;
    if (config.type == DataSourceType::SINGLE_DATA)
    {
        m_fieldPrefix = config.destinationPrefix;
        m_fieldList[config.fromName] = config.destination;
    }
    else if (config.type == DataSourceType::ALL_DATA)
    {
        m_fieldPrefix = config.destinationPrefix;
    }
    else if (config.type == DataSourceType::VALUE)
    {
        m_destination = config.destination;
    }
}

ADDHClientHandler::~ADDHClientHandler()
{
    // Clean warning before destroying this handler
    warningSignal(m_index, std::string());
    errorSignal(m_index, std::string());
}

void ADDHClientHandler::infoHandler()
{
    // Callback for DataSourceType::VALUE
    try
    {
        if (itsService->getSize() <= 0)
        {
            // No Link
            warningSignal(m_index, "No Link from service: " + m_service);
            m_inError = true;
        }
        else
        {
            warningSignal(m_index, std::string()); // Clear warning
            m_inError = false;
            std::lock_guard<std::mutex> lock(m_mutex);
            m_data.clear();
            FieldList values = makeValue(m_destination);
            m_data.insert(std::end(m_data), std::begin(values),
                          std::end(values));
        }
    }
    catch (const ADDHException &ex)
    {
        std::ostringstream str;
        str << "Exception while creating ADDH Header: " << ex.what();
        warningSignal(m_index, str.str());
        m_inError = true;
        LOG(ERROR) << "[ADDHClient] ADDHException: " << str.str();
    }
    catch (const std::exception &ex)
    {
        LOG(ERROR) << "[ADDHClient] Caught std exception " << ex.what();
    }
}

void ADDHClientHandler::errorReceived(
    const std::string &errMsg,
    const ntof::dim::DIMProxyClient & /*client*/)
{
    m_inError = true;
    if (errMsg == dim::DIMProxyClient::NoLinkError)
        warningSignal(m_index, "No Link from service: " + m_service);
    else
        warningSignal(m_index, "Error received from " + m_service);
}

void ADDHClientHandler::dataChanged(std::vector<ntof::dim::DIMData> &dataSet,
                                    const ntof::dim::DIMProxyClient & /*client*/)
{
    // Callback for DataSourceType::SINGLE_DATA and DataSourceType::ALL_DATA
    if (!dataSet.empty())
    {
        warningSignal(m_index, std::string()); // Clear warning
        m_inError = false;
        try
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            m_data.clear();
            std::vector<ntof::dim::DIMData>::const_iterator it;
            if (m_fieldList.empty())
            {
                // Complete dataset to be stored into ADDH
                for (it = dataSet.begin(); it != dataSet.end(); ++it)
                {
                    m_data.push_back(std::move(
                        makeValue((m_fieldPrefix + it->getName()), *it)));
                }
            }
            else
            {
                // Partial saving of dataset
                for (it = dataSet.begin(); it != dataSet.end(); ++it)
                {
                    FieldMap::iterator fieldIt = m_fieldList.find(it->getName());
                    if (fieldIt != m_fieldList.end())
                    {
                        m_data.push_back(
                            std::move(makeValue(fieldIt->second, *it)));
                    }
                }
            }
        }
        catch (const ADDHException &ex)
        {
            std::ostringstream str;
            str << "Exception while creating ADDH Header: " << ex.what();
            warningSignal(m_index, str.str());
            m_inError = true;
            LOG(ERROR) << "[ADDHClient] ADDHException: " << str.str();
        }
    }
}

template<typename DestType, typename SourceType>
ADDHClientHandler::FieldList ADDHClientHandler::createADDH(
    const std::string &name,
    AdditionalDataValue::Type type,
    const SourceType *data,
    size_t rows,
    size_t columns)
{
    FieldList ret;
    for (int i = 0; i < rows; i++)
    {
        std::string rowName(name);
        if (rows > 1)
            rowName += "/Row" + std::to_string(i);
        FieldList::value_type val(
            new AdditionalDataValue(rowName, type, columns));
        for (int j = 0; j < columns; j++)
        {
            val->append<DestType>(data[i * columns + j]);
        }
        ret.push_back(std::move(val));
    }
    return ret;
}

bool ADDHClientHandler::isFormatSupported(size_t &rows, size_t &columns)
{
    std::string format(this->itsService->getFormat());

    // if "|" is present, it means service is not supported (CMD or RPC)
    if (format.find('|') != std::string::npos)
    {
        return false;
    }

    // Example of supported multi-array: "D:3;D3"
    // Example of supported array: "D", "D:3"
    // Example of not supported format: "D:3;D:4" -> dimensions are different
    //                                  "D:3;F:3" -> type are different
    //                                  "D:3;D:3;D" -> last array unknown length

    rows = 1;
    columns = 0;
    size_t count = 0;
    // Get all the items
    int size = this->itsService->getSize();
    if (format.at(0) == 'I' || format.at(0) == 'L')
        count = size / sizeof(int32_t);
    else if (format.at(0) == 'C')
        count = size;
    else if (format.at(0) == 'X')
        count = size / sizeof(int64_t);
    else if (format.at(0) == 'S')
        count = size / sizeof(int16_t);
    else if (format.at(0) == 'F')
        count = size / sizeof(float);
    else if (format.at(0) == 'D')
        count = size / sizeof(double);
    else
    {
        return false;
    } // Format not supported

    if (format.size() == 1)
    { // Basic Format
        columns = count;
        return true;
    }

    // Save the first type
    char &dimType = format.front();

    std::vector<std::string> allFormats;
    boost::split(allFormats, format, boost::is_any_of(";"));
    rows = allFormats.size();
    for (std::string &f : allFormats)
    {
        std::vector<std::string> parts; // part[0] -> dimType, part[1] -> columns
        boost::split(parts, f, boost::is_any_of(":"));
        if (parts.size() != 2)
            return false; // Strange format
        if (dimType != f.front())
            return false; // Different types
        size_t cols;
        std::istringstream iss(parts[1]);
        iss >> cols;
        // First time
        if (columns == 0)
            columns = cols;
        if (columns != cols)
            return false; // multi array of different sizes
    }

    // Check if size count is equal to rows * columns
    if (count != rows * columns)
    {
        std::ostringstream oss;
        oss << "Size doesn't reflect format structure for: " << m_destination
            << ". Format is: " << format << "."
            << " Count: " << count << ". Rows: " << rows
            << ". Columns: " << columns;
        ADDH_THROW(oss.str(), 0);
    }
    return true;
}

ADDHClientHandler::FieldList ADDHClientHandler::makeValue(const std::string &name)
{
    ntof::dim::DIMLockGuard dimLock;
    std::string format(this->itsService->getFormat());
    size_t rows, columns;
    if (!isFormatSupported(rows, columns))
    {
        std::ostringstream oss;
        oss << "Unsupported Dim format structure for: " << m_destination
            << ". Format is: " << format;
        ADDH_THROW(oss.str(), 0);
    }
    char *data = (char *) this->itsService->getData();

    if (format.at(0) == 'I' || format.at(0) == 'L')
    { // L is deprecated, here for backward-compatibility
        return createADDH<int32_t, int32_t>(
            name, AdditionalDataValue::TypeInt32,
            reinterpret_cast<const int32_t *>(data), rows, columns);
    }
    else if (format.at(0) == 'C')
    {
        FieldList::value_type val(new AdditionalDataValue(
            name, AdditionalDataValue::TypeChar, columns));
        val->fromString(this->itsService->getString());
        FieldList ret;
        ret.push_back(val);
        return ret;
    }
    else if (format.at(0) == 'X')
    {
        return createADDH<int64_t, int64_t>(
            name, AdditionalDataValue::TypeInt64,
            reinterpret_cast<const int64_t *>(data), rows, columns);
    }
    else if (format.at(0) == 'S')
    {
        return createADDH<int32_t, int16_t>(
            name, AdditionalDataValue::TypeInt32,
            reinterpret_cast<const int16_t *>(data), rows, columns);
    }
    else if (format.at(0) == 'F')
    {
        return createADDH<float, float>(name, AdditionalDataValue::TypeFloat,
                                        reinterpret_cast<const float *>(data),
                                        rows, columns);
    }
    else if (format.at(0) == 'D')
    {
        return createADDH<double, double>(
            name, AdditionalDataValue::TypeDouble,
            reinterpret_cast<const double *>(data), rows, columns);
    }

    std::ostringstream oss;
    oss << "Unsupported Service Type for Basic Dim Service for: "
        << m_destination << ". Format is: " << format;
    ADDH_THROW(oss.str(), 0);
}

ADDHClientHandler::FieldList::value_type ADDHClientHandler::makeValue(
    const std::string &name,
    const ntof::dim::DIMData &data)
{
    switch (data.getDataType())
    {
    case ntof::dim::DIMData::TypeString: {
        const std::string val(data.getStringValue());
        FieldList::value_type ret(new AdditionalDataValue(
            name, AdditionalDataValue::TypeChar, val.length()));
        ret->fromString(val);
        return ret;
    }
    case ntof::dim::DIMData::TypeInt: {
        FieldList::value_type ret(
            new AdditionalDataValue(name, AdditionalDataValue::TypeInt32, 1));
        ret->append<int32_t>(data.getIntValue());
        return ret;
    }
    case ntof::dim::DIMData::TypeShort: {
        FieldList::value_type ret(
            new AdditionalDataValue(name, AdditionalDataValue::TypeInt32, 1));
        ret->append<int32_t>(data.getShortValue());
        return ret;
    }
    case ntof::dim::DIMData::TypeLong: {
        FieldList::value_type ret(
            new AdditionalDataValue(name, AdditionalDataValue::TypeInt64, 1));
        ret->append<int64_t>(data.getLongValue());
        return ret;
    }
    case ntof::dim::DIMData::TypeFloat: {
        FieldList::value_type ret(
            new AdditionalDataValue(name, AdditionalDataValue::TypeFloat, 1));
        ret->append<float>(data.getFloatValue());
        return ret;
    }
    case ntof::dim::DIMData::TypeDouble: {
        FieldList::value_type ret(
            new AdditionalDataValue(name, AdditionalDataValue::TypeDouble, 1));
        ret->append<double>(data.getDoubleValue());
        return ret;
    }
    default: {
        std::ostringstream oss;
        oss << "Unsupported data type: " << data.getDataType();
        ADDH_THROW(oss.str(), 0);
    }
    }
}

ADDHClientHandler::FieldList ADDHClientHandler::getADDHData()
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_data;
}

bool ADDHClientHandler::isInError() const
{
    return m_inError;
}

void ADDHClientHandler::addField(const std::string &from,
                                 const std::string &destination)
{
    if (!m_fieldList.empty())
    {
        m_fieldList[from] = destination;
    }
}

void ADDHClientHandler::addAllFields()
{
    m_fieldList.clear();
}

} // namespace addh
} // namespace ntof

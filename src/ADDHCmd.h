/*
 * ADDHCmd.h
 *
 *  Created on: Oct 23, 2014
 *      Author: agiraud
 */

#ifndef ADDHCMD_H_
#define ADDHCMD_H_

#include <algorithm>
#include <iostream>
#include <string>

#include <Signals.hpp>
#include <easylogging++.h>

#include "ADDHTypes.hpp"
#include "DIMSuperCommand.h"

namespace ntof {
namespace addh {

/**
 * @class ADDHCmd
 * @brief class to receive a the DIM Super Command
 *  The class catch the command and send the action contained inside to the addh
 */
class ADDHCmd : public ntof::dim::DIMSuperCommand
{
public:
    typedef ntof::utils::signal<void(const ADDHCommand &cmd)>
        CommandReceivedSignal;
    /**
     *  @brief Constructor of the class ADDHCmd
     */
    ADDHCmd();

    /**
     *  @brief Destructor of the class ADDHCmd
     */
    ~ADDHCmd() override = default;

    static bool isValidPath(const std::string &filePath,
                            const std::string &m_filePathPrefix);

    /**
     *  @brief Receive all the commands send to the DIM Super Command. This
     *         method is executed each time a command is received
     */
    void commandReceived(ntof::dim::DIMCmd &cmdData) override;

    // Signal
    CommandReceivedSignal commandReceivedSignal;

protected:
    std::string m_filePathPrefix;
};
} // namespace addh
} // namespace ntof

#endif /* ADDHCMD_H_ */

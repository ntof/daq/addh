#include "ADDHException.h"

#include <sstream>

namespace ntof {
namespace addh {

ADDHException::ADDHException(const std::string &Msg,
                             const std::string &file,
                             int Line,
                             int error) :
    NTOFException(Msg, file, Line, error)
{}

ADDHException::ADDHException(const char *Msg,
                             const std::string &file,
                             int Line,
                             int error) :
    NTOFException(Msg, file, Line, error)
{}

const char *ADDHException::what() const throw()
{
    if (m_what.empty())
    {
        std::ostringstream oss;
        oss << "addh exception in source file " << getFile() << " at line "
            << getLine() << " : " << getMessage();
        m_what = oss.str();
    }
    return m_what.c_str();
}

} /* namespace addh */
} /* namespace ntof */

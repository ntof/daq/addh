/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-04-08T00:19:48+02:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include "ADDHTypes.hpp"

namespace ntof {
namespace addh {

static const std::string s_empty;
static const std::map<DataSourceType, std::string> s_DataSourceTypeMap{
    {SINGLE_DATA, "SINGLE_DATA"},
    {ALL_DATA, "ALL_DATA"},
    {VALUE, "VALUE"}};

const std::string &toString(DataSourceType type)
{
    std::map<DataSourceType, std::string>::const_iterator it =
        s_DataSourceTypeMap.find(type);

    return (it != s_DataSourceTypeMap.end()) ? it->second : s_empty;
}

} /* namespace addh */
} /* namespace ntof */

/*
 * ADDHWriter.cpp
 *
 *  Created on: Apr 4, 2016
 *      Author: agiraud
 */

#include "ADDHWriter.h"

#include <easylogging++.h>

#include "ADDH.h"
#include "ADDHCmd.h"
#include "ADDHException.h"
#include "Config.h"
#include "WriterFile.h"

#include <dis.hxx>

namespace ntof {
namespace addh {

ADDHWriter::ADDHWriter() : m_commands("ADDHCommand", 100)
{
    // Initialized DIM parameters
    DimServer::setDnsNode(Config::instance().getDimDns().c_str());
    DimClient::setDnsNode(Config::instance().getDimDns().c_str());

    DimServer::start(Config::instance().getValue("serverName", "ADDH").c_str());
    initServices();
}

ADDHWriter::~ADDHWriter()
{
    stop();
    clearServices();
}

void ADDHWriter::clearServices()
{
    {
        // safely disable any connections
        std::lock_guard<std::mutex> lock(m_mutex);
        m_conn.clear();
    }

    {
        m_addhCmd.reset();
        m_writer.reset();
        m_addh.reset();
        m_state.reset();
    }
}

void ADDHWriter::initServices()
{
    clearServices();

    // State
    m_state.reset(new ntof::dim::DIMState(
        Config::instance().getValue("stateServiceName", "ADDH/State")));
    m_state->addStateValue(UNDEFINED, "UNDEFINED");
    m_state->addStateValue(STARTED, "STARTED");
    setState(UNDEFINED);

    m_errSlot = [this](int32_t code, const std::string &message) {
        this->setError(code, message);
    };
    m_warnSlot = [this](int32_t code, const std::string &message) {
        this->setWarning(code, message);
    };

    // Other dependencies
    m_addh.reset(new ADDH());
    connect("addh", &m_addh->errorSignal, &m_addh->warningSignal);
    m_addh->initConfigurations();

    m_writer.reset(new WriterFile(m_addh));
    connect("writer", &m_writer->errorSignal, &m_writer->warningSignal);

    m_addhCmd.reset(new ADDHCmd());

    m_addhCmd->commandReceivedSignal.connect([this](const ADDHCommand &cmd) {
        m_commands.post(new ADDHCommand(cmd));
    });
}

void ADDHWriter::thread_func()
{
    setState(STARTED);

    while (m_started.load())
    {
        try
        {
            // Read the first command in the queue
            ADDHCommand *cmd = m_commands.pop(1000);
            doAction(cmd);
            delete cmd;
        }
        catch (const NTOFException &ex)
        {
            // Timeout, just do nothing.
        }
    }
}

void ADDHWriter::setState(int32_t val)
{
    m_state->setValue(val);
}

void ADDHWriter::setError(int32_t code, const std::string &message)
{
    if (message.empty())
    {
        if (m_state->removeError(code))
        {
            LOG(ERROR) << "[ADDH/State]: error cleared code=" << code;
        }
    }
    else
    {
        LOG(ERROR)
            << "[ADDH/State]: error code=" << code << " message=" << message;
        m_state->addError(code, message);
    }
}

void ADDHWriter::clearErrors()
{
    m_state->clearErrors();
}

void ADDHWriter::setWarning(int32_t code, const std::string &message)
{
    if (message.empty())
    {
        if (m_state->removeWarning(code))
        {
            LOG(WARNING) << "[ADDH/State]: warning cleared code=" << code;
        }
    }
    else
    {
        LOG(WARNING)
            << "[ADDH/State]: warning code=" << code << " message=" << message;
        m_state->addWarning(code, message);
    }
}

void ADDHWriter::clearWarnings()
{
    m_state->clearWarnings();
}

void ADDHWriter::doAction(ADDHCommand *command)
{
    if ((*command).command == "write")
    {
        actionWrite(command);
    }
    else if ((*command).command == "reset")
    {
        clearWarnings();
        clearErrors();
        // Reset ADDH Class
        disconnect("addh");
        m_addh.reset(new ADDH());
        connect("addh", &m_addh->errorSignal, &m_addh->warningSignal);
        m_addh->initConfigurations();
    }
}

void ADDHWriter::actionWrite(ADDHCommand *command)
{
    // write the ADDH in the .event file
    // std::string cmd = "addh";
    m_writer->addCommand((*command));
}

void ADDHWriter::connect(const std::string &key,
                         ErrorSignal *err,
                         WarningSignal *warn)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    if (err)
        m_conn.emplace(std::make_pair(key + "_err", err->connect(m_errSlot)));
    if (warn)
        m_conn.emplace(std::make_pair(key + "_warn", warn->connect(m_warnSlot)));
}

void ADDHWriter::disconnect(const std::string &key)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    m_conn.erase(key + "_err");
    m_conn.erase(key + "_warn");
}

} /* namespace addh */
} /* namespace ntof */

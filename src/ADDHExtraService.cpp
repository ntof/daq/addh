/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-04-12T00:09:23+02:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include "ADDHExtraService.hpp"

#include <pugixml.hpp>

#include "ADDHException.h"
#include "Config.h"

template class ntof::utils::Singleton<ntof::addh::ADDHExtraService>;

namespace ntof {
namespace addh {

ADDHExtraService::ADDHExtraService() :
    m_service(new ntof::dim::DIMXMLService(
        Config::instance().getValue("exrtaServiceName", "ADDH/Extra")))
{
    // Load Service from file
    loadConfig();
    // No need to send updateServicesSignal(), first time will be handled by
    // ADDH constructor.
    updateXMLService(false); // Do not save to file, we have just read it
}

void ADDHExtraService::updateXMLService(bool saveToFile)
{
    pugi::xml_document doc;
    generateXmlDoc(doc);
    m_service->setData(doc);
    if (saveToFile)
        saveConfig(doc);
}

void ADDHExtraService::loadConfig()
{
    const std::string &extraConf = Config::instance().getExtraConfigFile();
    pugi::xml_document doc;
    // Read the config file
    pugi::xml_parse_result result = doc.load_file(extraConf.c_str());
    if (!result)
    {
        // Missing configuration file, create an empty one!
        LOG(INFO) << "Creating missing extra configuration file " << extraConf;
        pugi::xml_document newDoc;
        generateXmlDoc(newDoc); // will be an empty <extra />
        saveConfig(newDoc);
    }
    else
    {
        const std::lock_guard<std::mutex> lock(m_mutex);
        pugi::xml_node extraNode = doc.child("extra");
        Config::loadServices(extraNode, m_extraConfigData);
    }
}

void ADDHExtraService::saveConfig(const pugi::xml_document &doc) const
{
    const std::string &extraConf = Config::instance().getExtraConfigFile();
    if (!doc.save_file(extraConf.c_str()))
    {
        // Failed to save config
        LOG(ERROR)
            << "[ERROR] Can't save extra configuration to: " << extraConf;
    }
}
void ADDHExtraService::generateXmlDoc(pugi::xml_document &doc)
{
    doc.reset();
    pugi::xml_node extraNode = doc.append_child("extra");
    for (const ADDHClientConfig &config : m_extraConfigData)
    {
        const std::string nodeName = config.type == DataSourceType::VALUE ?
            "value" :
            "data";
        pugi::xml_node node = extraNode.append_child(nodeName.c_str());
        switch (config.type)
        {
        case DataSourceType::ALL_DATA:
            node.append_attribute("fromService")
                .set_value(config.fromService.c_str());
            node.append_attribute("destinationPrefix")
                .set_value(config.destinationPrefix.c_str());
            break;
        case DataSourceType::SINGLE_DATA:
            node.append_attribute("fromService")
                .set_value(config.fromService.c_str());
            node.append_attribute("fromName").set_value(config.fromName.c_str());
            node.append_attribute("destination")
                .set_value(config.destination.c_str());
            break;
        case DataSourceType::VALUE:
            node.append_attribute("fromService")
                .set_value(config.fromService.c_str());
            node.append_attribute("destination")
                .set_value(config.destination.c_str());
            break;
        default:
            LOG(ERROR) << "[ERROR] Unknown config type: " << config.type;
            break;
        }
    }
}

bool ADDHExtraService::executeCommand(pugi::xml_node extraNode)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    Config::loadServices(extraNode, m_extraConfigData);
    lock.unlock();
    updateServicesSignal();
    lock.lock();
    updateXMLService();
    return true;
}

const ClientsConfigList &ADDHExtraService::getExtraData() const
{
    const std::lock_guard<std::mutex> lock(m_mutex);
    return m_extraConfigData;
}

} /* namespace addh */
} /* namespace ntof */

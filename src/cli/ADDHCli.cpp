/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-15T09:03:43+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include "ADDHCli.hpp"

#include <DIMException.h>
#include <easylogging++.h>
#include <pugixml.hpp>

using namespace ntof::addh;
using namespace ntof::dim;

ADDHCli::ADDHCli() : m_cmd("ADDH/Command") {}

bool ADDHCli::cmdReset()
{
    return sendSimpleCommand("reset");
}

bool ADDHCli::cmdWrite(int32_t runNumber,
                       int32_t timingEvent,
                       const std::string &filePath)
{
    pugi::xml_document doc;
    pugi::xml_node command = doc.append_child("command");
    command.append_attribute("name").set_value("write");
    command.append_attribute("runNumber").set_value(runNumber);
    command.append_attribute("timingEvent").set_value(timingEvent);
    command.append_attribute("filePath").set_value(filePath.c_str());
    return sendRawCommand(doc);
}

bool ADDHCli::cmdExtra(const pugi::xml_document &extraDoc)
{
    pugi::xml_document doc;
    pugi::xml_node command = doc.append_child("command");
    command.append_attribute("name").set_value("extra");
    command.append_copy(extraDoc.document_element());
    return sendRawCommand(doc);
}

bool ADDHCli::sendSimpleCommand(const std::string &cmd)
{
    pugi::xml_document doc;
    doc.append_child("command").append_attribute("name").set_value(cmd.c_str());
    return sendRawCommand(doc);
}

bool ADDHCli::sendRawCommand(pugi::xml_document &doc)
{
    try
    {
        std::cout << "Sending Command with document:\n";
        doc.save(std::cout);
        m_cmd.sendCommand(doc);
        return true;
    }
    catch (DIMException &ex)
    {
        LOG(DEBUG) << "Command failed: " << ex.what();
        return false;
    }
}

void ADDHCli::listen()
{
    if (m_eventCli)
        return;

    m_eventCli.reset(new ntof::dim::DIMXMLInfo());
    m_eventCli->subscribe("ADDH/Event");
    m_eventCli->dataSignal.connect(
        [this](pugi::xml_document &doc, DIMXMLInfo & /*info*/) {
            m_lastEventData.reset(doc);
        });
    m_eventCli->dataSignal.connect(eventSignal);

    m_extraCli.reset(new DIMXMLInfo());
    m_extraCli->subscribe("ADDH/Extra");
    m_extraCli->dataSignal.connect(
        [this](pugi::xml_document &doc, DIMXMLInfo & /*info*/) {
            m_lastExtraData.reset(doc);
        });
    m_extraCli->dataSignal.connect(extraSignal);

    m_addhState.reset(new DIMStateClient("ADDH/State"));
    m_addhState->dataSignal.connect(addhStateSignal);
}

const pugi::xml_document &ADDHCli::getEventDataDoc() const
{
    return m_lastEventData;
}

const pugi::xml_document &ADDHCli::getExtraDataDoc() const
{
    return m_lastExtraData;
}

ADDHState ADDHCli::getAddhState() const
{
    if (m_addhState)
        return static_cast<ADDHState>(m_addhState->getActualState());
    return static_cast<ADDHState>(-3); // doesn't exist
}

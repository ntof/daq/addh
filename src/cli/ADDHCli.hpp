/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-15T09:03:43+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#ifndef ADDHCLI_HPP__
#define ADDHCLI_HPP__

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include <DIMStateClient.h>
#include <DIMSuperClient.h>
#include <DIMXMLInfo.h>

#include "ADDHTypes.hpp"

namespace ntof {
namespace addh {

class ADDHCli
{
public:
    typedef ntof::dim::DIMXMLInfo::DataSignal XMLDataSignal;
    typedef ntof::dim::DIMStateClient::DataSignal StateSignal;

    ADDHCli();

    bool cmdReset();
    bool cmdWrite(int32_t runNumber,
                  int32_t timingEvent,
                  const std::string &filePath);
    bool cmdExtra(const pugi::xml_document &extraDoc);

    void listen();

    const pugi::xml_document &getEventDataDoc() const;
    const pugi::xml_document &getExtraDataDoc() const;

    ADDHState getAddhState() const;

    inline ntof::dim::DIMStateClient *getStateClient() const
    {
        return m_addhState.get();
    };

    XMLDataSignal eventSignal;
    XMLDataSignal extraSignal;
    StateSignal addhStateSignal;

protected:
    bool sendSimpleCommand(const std::string &cmd);
    bool sendRawCommand(pugi::xml_document &doc);

    ntof::dim::DIMSuperClient m_cmd;
    std::unique_ptr<ntof::dim::DIMXMLInfo> m_eventCli;
    pugi::xml_document m_lastEventData;
    std::unique_ptr<ntof::dim::DIMXMLInfo> m_extraCli;
    pugi::xml_document m_lastExtraData;
    std::unique_ptr<ntof::dim::DIMStateClient> m_addhState;
};

} // namespace addh
} // namespace ntof

#endif

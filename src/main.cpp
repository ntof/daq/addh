/**
 * \file main.cpp
 * \brief Main system
 * \author A. GIRAUD
 * \version 1.0
 * \date Sep 24, 2014
 *
 * Start the system.
 *
 */

#include "ADDHWriter.h"
#include "Config.h"

// Include logging stuff
#include <easylogging++.h>

#include "ADDHException.h"

/**
 * \brief Main function
 * \param argc: number of parameters
 * \param argv: list of parameters
 * \return Return 0
 */
int main(int argc, char **argv)
{
    START_EASYLOGGINGPP(argc, argv);
    try
    {
        const std::string configFile = (argc > 1) ?
            std::string(argv[1]) :
            ntof::addh::Config::configFile;
        const std::string extraConfigFile = (argc > 2) ?
            std::string(argv[2]) :
            ntof::addh::Config::extraConfigFile;
        const std::string configMiscFile = (argc > 3) ?
            std::string(argv[3]) :
            ntof::addh::Config::configMiscFile;

        ntof::addh::Config::load(configFile, extraConfigFile, configMiscFile);

        // Get the path of the config file of the logger
        std::string logConfFile = "/etc/ntof/nTOF_ADDH_logger.conf";
        if (argc > 4)
        {
            logConfFile = argv[4];
        }

        // Initialize logger
        el::Configurations confFromFile(
            logConfFile); // Load configuration from file
        el::Loggers::reconfigureAllLoggers(
            confFromFile); // Re-configures all the loggers to current
                           // configuration file

        // Run ADDH Writer
        LOG(INFO) << "ADDH Writer starting";
        ntof::addh::ADDHWriter addhWriter;
        addhWriter.run();
    }
    catch (const ntof::addh::ADDHException &ex)
    {
        LOG(ERROR)
            << "[FATAL ERROR] Caught addh exception : " << ex.getMessage();
        return -1;
    }
    catch (const std::exception &ex)
    {
        LOG(ERROR) << "[FATAL ERROR] Caught exception : " << ex.what();
        return -1;
    }
    catch (...)
    {
        LOG(ERROR) << "[FATAL ERROR] Caught unknown exception";
        return -1;
    }

    return 0;
}

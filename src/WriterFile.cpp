/*
 * WriterFile.cpp
 *
 *  Created on: Aug 1, 2014
 *      Author: agiraud
 */

#include "WriterFile.h"

#include <ios>
#include <map>

#include <DaqTypes.h>
#include <Synchro.h>
#include <fcntl.h>
#include <sys/types.h>

#include "ADDHClientHandler.h"
#include "ADDHException.h"
#include "ADDHWriter.h"
#include "Config.h"

namespace ntof {
namespace addh {

void WriterFile::threadProc()
{
    try
    {
        while (true)
        {
            // Read the first command in the queue
            ADDHCommand *cmd = commands_.pop();
            doAction(*cmd);
            delete cmd;
        }
    }
    catch (ntof::NTOFException &ex)
    {
        LOG(WARNING) << "WriterFile thread stopped: " << ex.what();
    }
}

void WriterFile::addCommand(const ADDHCommand &command)
{
    commands_.post(new ADDHCommand(command));
}

WriterFile::WriterFile(const std::shared_ptr<ADDH> &addh) :
    commands_("ADDHWriterCommands", 50),
    m_svcEvents(Config::instance().getValue("eventsServiceName", "ADDH/Events")),
    m_addh(addh)
{
    if (!m_addh)
        errorSignal(INTERNAL_ERR,
                    "intiailization error in WriterFile ADDH object missing.");
    else
    {
        m_thread.reset(new std::thread(&WriterFile::threadProc, this));
    }
}

WriterFile::~WriterFile()
{
    commands_.clear(true);
    if (m_thread)
    {
        m_thread->join();
    }
}

void WriterFile::doAction(const ADDHCommand &command)
{
    if (command.command == "write")
    {
        writeFileEvent(command);
    }
    else
    {
        errorSignal(WRITE_ERR, "ADDH writer internal error : action unknown.");
    }
}

void WriterFile::dumpADDHData(const ClientsMap &clients,
                              std::ofstream &out) const
{
    for (const ClientsMap::value_type &client : clients)
    {
        if (client.second.client->isInError())
            continue;

        const ADDHClientHandler::FieldList fields =
            client.second.client->getADDHData();
        for (const std::shared_ptr<AdditionalDataValue> &field : fields)
        {
            out << (*field);
        }
    }
}

void WriterFile::writeFileEvent(const ADDHCommand &command)
{
    std::ofstream out;
    /* stl always truncates if in not set */
    out.open(command.filePath.c_str(), std::ios_base::out | std::ios_base::in);
    if (!out.good())
    {
        /* create the file */
        out.open(command.filePath.c_str(), std::ios_base::out);
    }
    if (!out.good())
    {
        errorSignal(WRITE_ERR,
                    "ADDH writer internal error : unable to open the file.");
        return;
    }

    out.seekp(0, std::ios_base::end);
    {
        AdditionalDataHeader hdr;
        out << hdr; /* dump an empty header */
    }

    std::streampos contentPos = out.tellp();

    // dump lsaCycle
    if (!command.lsaCycle.empty())
    {
        AdditionalDataValue lsaCycleValue("lsaCycle",
                                          AdditionalDataValue::TypeChar,
                                          command.lsaCycle.length());
        lsaCycleValue.fromString(command.lsaCycle);
        out << lsaCycleValue;
    }
    // dump user
    if (!command.user.empty())
    {
        AdditionalDataValue userValue("user", AdditionalDataValue::TypeChar,
                                      command.user.length());
        userValue.fromString(command.user);
        out << userValue;
    }

    const ClientsMap clients = m_addh->getClients();
    dumpADDHData(clients, out);
    const ClientsMap extraClients = m_addh->getExtraClients();
    dumpADDHData(extraClients, out);

    std::streamoff contentSize = out.tellp() - contentPos;
    LOG(INFO) << "Addh content size: " << contentSize;

    size_t fileSize = out.tellp();
    out.seekp(contentPos - std::streamoff(sizeof(int32_t)));
    int32_t size = contentSize / 4;
    out.write(reinterpret_cast<const char *>(&size), sizeof(int32_t));

    if (!out.good())
    {
        errorSignal(WRITE_ERR, "Failed to write ADDH");
    }

    out.close();
    sendEvent(command, fileSize);
}

void WriterFile::sendEvent(const ADDHCommand &cmd, size_t fileSize)
{
    LOG(INFO) << "sending event for " << cmd.filePath << " size:" << fileSize;

    // Build XML data to be sent
    pugi::xml_document doc;
    pugi::xml_node root = doc.append_child("addh");
    pugi::xml_node event = root.append_child("event");
    event.append_attribute("runNumber").set_value(cmd.runNumber);
    event.append_attribute("timingEvent").set_value(cmd.timingEvent);
    event.append_attribute("filePath").set_value(cmd.filePath.c_str());
    event.append_attribute("fileSize").set_value(fileSize);

    /* send event */
    m_svcEvents.setData(doc);
}

} // namespace addh
} // namespace ntof

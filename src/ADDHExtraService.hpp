/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-04-12T00:09:23+02:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#ifndef ADDH_ADDHEXTRASERVICE_HPP
#define ADDH_ADDHEXTRASERVICE_HPP

#include <memory>

#include <DIMXMLService.h>
#include <Signals.hpp>

#include "ADDHTypes.hpp"

#include <Singleton.hxx>

namespace ntof {
namespace addh {

class ADDHExtraService : public ntof::utils::Singleton<ADDHExtraService>
{
public:
    typedef ntof::utils::signal<void()> UpdateExtraServicesSignal;

    /**
     *  @return the client configs
     */
    const ClientsConfigList &getExtraData() const;

    /**
     *  @brief "extra" command from ADDH/Cmd
     *  @return true if command executed successfully
     */
    bool executeCommand(pugi::xml_node node);

    // Signal
    UpdateExtraServicesSignal updateServicesSignal;

protected:
    friend class Singleton<ADDHExtraService>;

    ADDHExtraService();

    /**
     *  @brief generate xml from config data
     *  @param[out] doc document to be filled in
     */
    void generateXmlDoc(pugi::xml_document &doc);

    /**
     *  @brief load configuration from file
     */
    void loadConfig();

    /**
     *  @brief save xml content do config file
     *  @param[in] doc xml document to be saved
     */
    void saveConfig(const pugi::xml_document &doc) const;

    /**
     *  @brief Update ADDH/Extra service content
     *  @param[in] saveToFile if true save configuration to file
     */
    void updateXMLService(bool saveToFile = true);

    mutable std::mutex m_mutex; //!< Mutex used to avoid concurrent access
    std::unique_ptr<ntof::dim::DIMXMLService> m_service; //!< Service where extra
                                                         //!< conf is published
    ClientsConfigList m_extraConfigData; //!< Collection of the ADDH fields
};

} /* namespace addh */
} /* namespace ntof */

#endif // ADDH_ADDHEXTRASERVICE_HPP

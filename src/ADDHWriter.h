/*
 * ADDWriter.h
 *
 *  Created on: Apr 4, 2016
 *      Author: agiraud
 */

#ifndef ADDHWRITER_H_
#define ADDHWRITER_H_

#include <cstdint>
#include <memory>

#include <DIMState.h>
#include <DIMSuperClient.h>
#include <DIMUtils.hpp>
#include <Queue.h>
#include <Signals.hpp>
#include <Thread.hpp>

#include "ADDHTypes.hpp"

namespace ntof {
namespace addh {
class ADDH;
class WriterFile;
class ADDHCmd;

/**
 * @class addh
 * @brief Main class of the addh system
 */
class ADDHWriter : public ntof::utils::Thread
{
public:
    ADDHWriter();

    /**
     *  @brief Destructor of the class addh
     */
    ~ADDHWriter() override;

    /**
     * @brief Init of EACS services
     */
    void initServices();

    /**
     *  @brief Change the value of the state machine
     *  @param val: new value to set. This value is a enum addhState
     */
    void setState(int32_t val);

    /* ******************************************/
    /* ERRORS                                   */
    /* ******************************************/

    /**
     * @brief set or clear an error
     * @param[in] code the error identifier
     * @param[in] message the error message, leave empty to clear the error
     */
    void setError(int32_t code, const std::string &message);

    /**
     *  @brief Remove all the errors from the state machine
     */
    void clearErrors();

    /**
     * @brief set or clear a warning
     * @param[in] code the warning identifier
     * @param[in] message the warning message, leave empty to clear the warning
     */
    void setWarning(int32_t code, const std::string &message);

    /**
     *  @brief Remove all the warnings from the state machine
     */
    void clearWarnings();

    /**
     *  @brief for test purpose
     */
    const std::shared_ptr<ADDH> &getAddh() { return m_addh; }

    /**
     * @brief connect an error signal to this state machine
     */
    void connect(const std::string &key,
                 ErrorSignal *err,
                 WarningSignal *warn = nullptr);

    /**
     * @brief disconnect an error signal to this state machine
     */
    void disconnect(const std::string &key);

protected:
    /**
     * @brief Launch the thread
     */
    void thread_func() override;

    /* ******************************************/
    /* ACTIONS                                  */
    /* ******************************************/

    /**
     *  @brief Execute the action when a new command is received
     *  @param command: the command name
     */
    void doAction(ADDHCommand *command);

    /**
     *  @brief Actions to perform when a command start is receivedactionWrite
     */
    void actionWrite(ADDHCommand *command);

    /**
     * @brief clear internals
     */
    void clearServices();

    ntof::utils::Queue<ADDHCommand *> m_commands; //!< The list of the
                                                  //!< commands received by
                                                  //!< the ADDH Writer
    std::shared_ptr<ADDH> m_addh;
    std::shared_ptr<WriterFile> m_writer;
    std::shared_ptr<ADDHCmd> m_addhCmd; //!< Client which receive the commands
    std::shared_ptr<ntof::dim::DIMState> m_state; //!< State machine

    std::function<ErrorSignal::signature_type> m_errSlot;
    std::function<WarningSignal::signature_type> m_warnSlot;

    // protected with Thread::m_mutex
    std::map<std::string, ntof::dim::scoped_connection> m_conn;
};

} /* namespace addh */
} /* namespace ntof */

#endif /* ADDHWRITER_H_ */

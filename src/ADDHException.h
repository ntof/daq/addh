/*
 * AddhException.h
 *
 *  Created on: May 8, 2015
 *      Author: mdonze
 */

#ifndef ADDHEXCEPTION_H_
#define ADDHEXCEPTION_H_

#include <NTOFException.h>

namespace ntof {
namespace addh {

#define ADDH_THROW(msg, code) throw ADDHException(msg, __FILE__, __LINE__, code)

/**
 * @class AddhException
 * @brief Exception throw by the addh
 */
class ADDHException : public NTOFException
{
public:
    /**
     * @brief Constructor of the class
     * @param Msg Error message
     * @param file Name of the file where the exception is thrown (__FILE__)
     * @param Line Line of code doing the error (__LINE__)
     */
    ADDHException(const std::string &Msg,
                  const std::string &file,
                  int Line,
                  int error = 0);

    /**
     * @brief Constructor of the class
     * @param Msg Error message
     * @param file Name of the file where the exception is thrown (__FILE__)
     * @param Line Line of code doing the error (__LINE__)
     * @param error Error code
     */
    ADDHException(const char *Msg,
                  const std::string &file,
                  int Line,
                  int error = 0);

    /**
     * @brief Destructor of the class
     */
    virtual ~ADDHException() = default;

    /**
     * @brief Overload of std::exception
     * @return Error message
     */
    virtual const char *what() const throw();

protected:
    mutable std::string m_what;
};

} /* namespace addh */
} /* namespace ntof */

#endif /* ADDHEXCEPTION_H_ */

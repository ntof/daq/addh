#ifndef ADDH_H_
#define ADDH_H_

#include <mutex>

#include <DIMUtils.hpp>
#include <DaqTypes.h>

#include "ADDHTypes.hpp"

namespace ntof {
namespace addh {

/**
 * @class ADDH
 * @brief The class which publish the ADDH_HEADER. The class get all the data
 * needed to generate the ADDH_HEADER
 */
class ADDH
{
public:
    ADDH() = default;
    ~ADDH();

    /**
     *  @brief It loads global and extra configurations
     */
    void initConfigurations();

    /**
     *  @brief Allow to get the collection of the clients
     *  @return The collection of clients
     */
    const ClientsMap getClients() const;

    /**
     *  @brief Allow to get the collection of the clients for extra config
     *  @return The collection of clients
     */
    const ClientsMap getExtraClients() const;

    ErrorSignal errorSignal;
    WarningSignal warningSignal;

protected:
    /**
     *  @brief iterate over global config and create clients
     */
    void handleGlobalConfig();

    /**
     *  @brief iterate over extra config and create clients
     */
    void handleExtraConfig();

    /**
     *  @brief utility to create ADDH clients
     *  @param configs[in] configuration of the addh data to manage
     *  @param clients[out] clients handlers for the configured services
     *  @param index[in] starting index used as warnings code in case of noLink
     */
    void createClients(const ClientsConfigList &configs,
                       ClientsMap &clients,
                       int32_t index = 0) const;

    mutable std::mutex m_mutex; //!< Mutex used to avoid concurrent access
    ClientsMap m_clients;       //!< List of the clients
    ClientsMap m_extraClients;  //!< List of the clients of extra configuration
    ntof::dim::scoped_connection m_extraConn; //!< Extra service connection
};

} // namespace addh
} // namespace ntof

#endif /* ADDH_H_ */

# ADDH

# Build
Only needed first time to fetch the docker image

```
docker pull $(cat .docker-builder)
```

Start a temporary container, binding current directory inside

```
docker run -v "$PWD:$PWD" -w "$PWD" --rm -it gitlab-registry.cern.ch/ntof/daq/builder /bin/bash
```

```
mkdir build && cd build
cmake3 .. -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCOVERAGE=ON -DTESTS=ON
make -j4
```

# Styling

```
make style
```

# Testing

```
cd tests
./test_all
```

/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-01-06T14:43:44+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <memory>

#include <boost/filesystem.hpp>

#include <DIMDataSet.h>
#include <DaqTypes.h>
#include <cppunit/TestFixture.h>

#include "ADDH.h"
#include "ADDHClientHandler.h"
#include "ADDHTypes.hpp"
#include "Config.h"
#include "local-config.h"
#include "test_helpers.hpp"

using namespace ntof::addh;

class TestADDH : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestADDH);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(updates);
    CPPUNIT_TEST(multipleFields);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() { m_dim.reset(new DimTestHelper()); }

    void tearDown() { m_dim.reset(); }

    void simple()
    {
        ADDH addh;
        addh.initConfigurations();
        const ClientsMap list = addh.getClients();

        EQ(std::size_t(45), list.size());

        ClientsMap::const_iterator it = list.find("HV_0/OnChannels");
        CPPUNIT_ASSERT(it != list.end());
        const ADDHClient &client = it->second;

        EQ(true, client.client->getADDHData().empty());
    }

    void updates()
    {
        ADDH addh;
        addh.initConfigurations();
        // don't ask about the const cast ...
        DimInfoWaiter info("HV_0/OnChannels");

        ntof::dim::DIMDataSet serv("HV_0/OnChannels");
        serv.addData("value", "V", int32_t(42), ntof::dim::AddMode::CREATE);

        EQ(true, info.waitUpdate());

        const ClientsMap list = addh.getClients();
        ClientsMap::const_iterator it = list.find("HV_0/OnChannels");
        CPPUNIT_ASSERT(it != list.end());
        const ADDHClient &client = it->second;

        const ADDHClientHandler::FieldList values = client.client->getADDHData();
        EQ(std::size_t(1), values.size());

        EQ(AdditionalDataValue::TypeInt32, values[0]->type);
        EQ(std::size_t(1), values[0]->count());
        EQ(int32_t(42), values[0]->at<int32_t>(0));
    }

    void multipleFields()
    {
        ADDH addh;
        addh.initConfigurations();
        DimInfoWaiter info("F16.BCT372");

        ntof::dim::DIMDataSet serv("F16.BCT372");
        serv.addData("totalIntensitySingle", "", int32_t(42),
                     ntof::dim::AddMode::CREATE, false);
        serv.addData("cycleTime", "", int32_t(43), ntof::dim::AddMode::CREATE,
                     false);
        serv.addData("beamDestinationStr", "", int32_t(44),
                     ntof::dim::AddMode::CREATE, true);

        EQ(true, info.waitUpdate());

        const ClientsMap list = addh.getClients();
        ClientsMap::const_iterator it = list.find("F16.BCT372");
        CPPUNIT_ASSERT(it != list.end());
        const ADDHClient &client = it->second;

        const ADDHClientHandler::FieldList values = client.client->getADDHData();
        EQ(std::size_t(3), values.size());

        EQ(int32_t(42), values[0]->at<int32_t>(0));
        EQ(int32_t(43), values[1]->at<int32_t>(0));
        EQ(int32_t(44), values[2]->at<int32_t>(0));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestADDH);

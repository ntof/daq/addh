/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-04-14T14:43:44+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include <memory>

#include <boost/filesystem.hpp>

#include <DIMDataSet.h>
#include <cppunit/TestFixture.h>

#include "ADDH.h"
#include "ADDHClientHandler.h"
#include "ADDHExtraService.hpp"
#include "ADDHTypes.hpp"
#include "ADDHWriter.h"
#include "Config.h"
#include "cli/ADDHCli.hpp"
#include "local-config.h"
#include "test_helpers.hpp"

using namespace ntof::addh;
using namespace ntof::utils;
namespace bfs = boost::filesystem;

class TestADDHWriter : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestADDHWriter);
    CPPUNIT_TEST(startup);
    CPPUNIT_TEST(resetCmd);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp()
    {
        bfs::remove(Config::instance().getExtraConfigFile());
        ADDHExtraService::destroy();
        m_dim.reset(new DimTestHelper());
    }

    void tearDown()
    {
        ADDHExtraService::destroy();
        m_dim.reset();
        bfs::remove(Config::instance().getExtraConfigFile());
    }

    void checkExtraXmlDoc(const pugi::xml_document &doc,
                          const std::string &fromService,
                          const std::string &destination)
    {
        EQ(false, doc.empty());
        pugi::xml_node extraNode = doc.child("extra");
        EQ(false, extraNode.empty());
        pugi::xml_node valueNode = extraNode.child("value");
        EQ(false, valueNode.empty());
        EQ(true,
           fromService == valueNode.attribute("fromService").as_string(""));
        EQ(true,
           destination == valueNode.attribute("destination").as_string(""));
    }

    void simple(ADDHWriter &addhWriter)
    {
        const std::shared_ptr<ADDH> &addh = addhWriter.getAddh();

        ADDHCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.addhStateSignal);
        SignalWaiter extraWaiter;
        extraWaiter.listen(cli.extraSignal);

        // Check State that is UNDEFINED
        EQ(true, stateWaiter.wait([&cli]() {
            return cli.getAddhState() == UNDEFINED;
        }));

        // Check global clients
        const ClientsMap clients = addh->getClients();
        for (const ClientsMap::value_type &client : addh->getClients())
        {
            EQ(true, client.second.client->isInError());
        }

        // Check extra clients
        const ClientsMap extraClients = addh->getExtraClients();
        EQ(size_t(0), extraClients.size());

        size_t numOfClients = clients.size();
        EQ(false, numOfClients == 0);

        // Check that all clients are in warning
        EQ(numOfClients, cli.getStateClient()->getActiveWarnings().size());

        // Start ADDH
        addhWriter.start();

        // Check State that is STARTED
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getAddhState() == STARTED; }));

        EQ(true, extraWaiter.wait(1)); // ADDH/Extra is empty

        // Create a services
        DimService service_I("TEST/Service_I", (int32_t) 10);
        DimService service_X("TEST/Service_X", (longlong) 20);

        // Check that warnings are now 2 less
        EQ(true, stateWaiter.wait([&cli, &numOfClients]() {
            return cli.getStateClient()->getActiveWarnings().size() ==
                (numOfClients - 2);
        }));

        // Check Extra configuration file content is empty
        pugi::xml_document docExtraFileEmpty;
        docExtraFileEmpty.load_file("/tmp/ntof_extra.xml");
        EQ(false, docExtraFileEmpty.empty());
        EQ(true, docExtraFileEmpty.child("value").empty());

        //  Add Extra Service via command
        pugi::xml_document doc;
        pugi::xml_node extra = doc.append_child("extra");
        pugi::xml_node extraValue = extra.append_child("value");
        extraValue.append_attribute("fromService").set_value("EXTRA/Service_I");
        extraValue.append_attribute("destination").set_value("ExtraService_I");
        cli.cmdExtra(doc);

        // Check that warnings are now 1 more (-2 + 1)
        EQ(true, stateWaiter.wait([&cli, &numOfClients]() {
            return cli.getStateClient()->getActiveWarnings().size() ==
                (numOfClients - 1);
        }));

        // Check extra clients are in error
        for (const ClientsMap::value_type &client : addh->getExtraClients())
        {
            EQ(true, client.second.client->isInError());
        }

        DimService service_I_extra("EXTRA/Service_I", (int32_t) 11);

        // Check that warnings are now 1 less
        EQ(true, stateWaiter.wait([&cli, &numOfClients]() {
            return cli.getStateClient()->getActiveWarnings().size() ==
                (numOfClients - 2);
        }));

        // Check extra clients are now NOT in error
        for (const ClientsMap::value_type &client : addh->getExtraClients())
        {
            EQ(false, client.second.client->isInError());
        }

        // Wait for Extra service update
        EQ(true, extraWaiter.wait(2));

        // Check ADDH/Extra service
        const pugi::xml_document &docExtra = cli.getExtraDataDoc();
        checkExtraXmlDoc(docExtra, "EXTRA/Service_I", "ExtraService_I");

        // Check Extra configuration file content
        pugi::xml_document docExtraFile;
        docExtraFile.load_file("/tmp/ntof_extra.xml");
        checkExtraXmlDoc(docExtraFile, "EXTRA/Service_I", "ExtraService_I");
    }

    void startup()
    {
        // Initialize ADDHWriter
        ADDHWriter addhWriter;
        addhWriter.initServices();

        simple(addhWriter);
    }

    void resetCmd()
    {
        // Initialize ADDHWriter
        ADDHWriter addhWriter;
        addhWriter.initServices();

        simple(addhWriter);

        ADDHCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.addhStateSignal);
        SignalWaiter extraWaiter;
        extraWaiter.listen(cli.extraSignal);

        std::shared_ptr<ADDH> addh(addhWriter.getAddh());
        const ClientsMap clients = addh->getClients();
        const ClientsMap clientsExtra = addh->getExtraClients();
        size_t numOfClients = clients.size() + clientsExtra.size();
        // Release addh shared ptr now
        addh.reset();

        EQ(true, numOfClients > 0);
        addhWriter.setWarning(1000, "Fake warning");

        // Check that warnings are clients + 1 (fake)
        EQ(true, stateWaiter.wait([&cli, &numOfClients]() {
            return cli.getStateClient()->getActiveWarnings().size() ==
                (numOfClients + 1);
        }));

        // Clear all the warnings
        cli.cmdReset();

        // Check that warnings are back after reset (with no fake one)
        EQ(true, stateWaiter.wait([&cli, &numOfClients]() {
            return cli.getStateClient()->getActiveWarnings().size() ==
                numOfClients;
        }));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestADDHWriter);

/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-01-06T14:43:44+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <memory>

#include <boost/filesystem.hpp>

#include <DIMDataSet.h>
#include <cppunit/TestFixture.h>

#include "ADDH.h"
#include "ADDHClientHandler.h"
#include "Config.h"
#include "local-config.h"
#include "test_helpers.hpp"

#include <dis.hxx>

using namespace ntof::addh;

class TestADDHBasicDim : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestADDHBasicDim);
    CPPUNIT_TEST(updatesSimpleValues);
    CPPUNIT_TEST(updateArrayValues);
    CPPUNIT_TEST(updateMultiArrayValues);
    CPPUNIT_TEST(updateStringValues);
    CPPUNIT_TEST(notSupportedStructure);
    CPPUNIT_TEST(notSupportedMultiArrayStructure);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() { m_dim.reset(new DimTestHelper()); }

    void tearDown() { m_dim.reset(); }

    template<typename T>
    void checkValues(const ADDH &addh,
                     const std::string &service,
                     T expectedValue,
                     AdditionalDataValue::Type expectedType)
    {
        const ClientsMap list = addh.getClients();
        ClientsMap::const_iterator it = list.find(service);
        CPPUNIT_ASSERT(it != list.end());
        const ADDHClient &client = it->second;

        const ADDHClientHandler::FieldList values = client.client->getADDHData();
        EQ(std::size_t(1), values.size());

        EQ(expectedType, values[0]->type);
        EQ(std::size_t(1), values[0]->count());
        if (expectedType == AdditionalDataValue::TypeFloat ||
            expectedType == AdditionalDataValue::TypeDouble)
            EQ_DBL(expectedValue, values[0]->at<T>(0), 0.01);
        else
            EQ(expectedValue, values[0]->at<T>(0));
    }

    template<typename T>
    void checkArrayValues(const ADDH &addh,
                          const std::string &service,
                          T expectedValue_0,
                          T expectedValue_1,
                          T expectedValue_2,
                          AdditionalDataValue::Type expectedType)
    {
        const ClientsMap list = addh.getClients();
        ClientsMap::const_iterator it = list.find(service);
        CPPUNIT_ASSERT(it != list.end());
        const ADDHClient &client = it->second;

        const ADDHClientHandler::FieldList values = client.client->getADDHData();
        EQ(std::size_t(1), values.size());

        EQ(expectedType, values[0]->type);
        EQ(std::size_t(3), values[0]->count());
        if (expectedType == AdditionalDataValue::TypeFloat ||
            expectedType == AdditionalDataValue::TypeDouble)
        {
            EQ_DBL(expectedValue_0, values[0]->at<T>(0), 0.01);
            EQ_DBL(expectedValue_1, values[0]->at<T>(1), 0.01);
            EQ_DBL(expectedValue_2, values[0]->at<T>(2), 0.01);
        }
        else
        {
            EQ(expectedValue_0, values[0]->at<T>(0));
            EQ(expectedValue_1, values[0]->at<T>(1));
            EQ(expectedValue_2, values[0]->at<T>(2));
        }
    }

    template<typename T>
    void checkMultyArrayValues(const ADDH &addh,
                               const std::string &service,
                               T expectedValue_0_0,
                               T expectedValue_0_1,
                               T expectedValue_1_0,
                               T expectedValue_1_1,
                               AdditionalDataValue::Type expectedType)
    {
        const ClientsMap list = addh.getClients();
        ClientsMap::const_iterator it = list.find(service);
        CPPUNIT_ASSERT(it != list.end());
        const ADDHClient &client = it->second;

        const ADDHClientHandler::FieldList values = client.client->getADDHData();
        EQ(std::size_t(2), values.size());

        // First Row
        EQ(expectedType, values[0]->type);
        EQ(std::size_t(2), values[0]->count());
        if (expectedType == AdditionalDataValue::TypeFloat ||
            expectedType == AdditionalDataValue::TypeDouble)
        {
            EQ_DBL(expectedValue_0_0, values[0]->at<T>(0), 0.01);
            EQ_DBL(expectedValue_0_1, values[0]->at<T>(1), 0.01);
        }
        else
        {
            EQ(expectedValue_0_0, values[0]->at<T>(0));
            EQ(expectedValue_0_1, values[0]->at<T>(1));
        }

        // Second Row
        EQ(expectedType, values[1]->type);
        EQ(std::size_t(2), values[1]->count());
        if (expectedType == AdditionalDataValue::TypeFloat ||
            expectedType == AdditionalDataValue::TypeDouble)
        {
            EQ_DBL(expectedValue_1_0, values[1]->at<T>(0), 0.01);
            EQ_DBL(expectedValue_1_1, values[1]->at<T>(1), 0.01);
        }
        else
        {
            EQ(expectedValue_1_0, values[1]->at<T>(0));
            EQ(expectedValue_1_1, values[1]->at<T>(1));
        }
    }

    void updatesSimpleValues()
    {
        ADDH addh;
        addh.initConfigurations();
        // don't ask about the const cast ...
        DimInfoWaiter info("TEST/Service_D");

        // Values to be published
        int32_t value_I = 10;
        longlong value_X = 30;
        int16_t value_S = 40;
        float value_F = 50.5f;
        double value_D = 60.60;

        // Basic Dim Service I
        DimService service_I("TEST/Service_I", value_I);
        // Basic Dim Service X
        DimService service_X("TEST/Service_X", value_X);
        // Basic Dim Service S
        DimService service_S("TEST/Service_S", value_S);
        // Basic Dim Service F
        DimService service_F("TEST/Service_F", value_F);
        // Basic Dim Service D
        DimService service_D("TEST/Service_D", value_D);

        EQ(true, info.waitUpdate());

        checkValues<int32_t>(addh, "TEST/Service_I", value_I,
                             AdditionalDataValue::TypeInt32);
        checkValues<int64_t>(addh, "TEST/Service_X", value_X,
                             AdditionalDataValue::TypeInt64);
        checkValues<int32_t>(addh, "TEST/Service_S", value_S,
                             AdditionalDataValue::TypeInt32);
        checkValues<float>(addh, "TEST/Service_F", value_F,
                           AdditionalDataValue::TypeFloat);
        checkValues<double>(addh, "TEST/Service_D", value_D,
                            AdditionalDataValue::TypeDouble);
    }

    void updateArrayValues()
    {
        ADDH addh;
        addh.initConfigurations();
        // don't ask about the const cast ...
        DimInfoWaiter info("TEST/Service_D");

        // Values to be published
        const int32_t value_I[3] = {1, 2, 3};
        longlong value_X[3] = {10, 20, 30};
        short value_S[3] = {11, 12, 13};
        float value_F[3] = {50.50f, 51.50f, 52.50f};
        double value_D[3] = {61.60, 62.60, 63.60};

        // Basic Dim Service I
        DimService service_I("TEST/Service_I", "I:3", &value_I, sizeof(value_I));
        // Basic Dim Service X
        DimService service_X("TEST/Service_X", "X", &value_X, sizeof(value_X));
        // Basic Dim Service S
        DimService service_S("TEST/Service_S", "S:3", &value_S, sizeof(value_S));
        // Basic Dim Service F
        DimService service_F("TEST/Service_F", "F", &value_F, sizeof(value_F));
        // Basic Dim Service D
        DimService service_D("TEST/Service_D", "D", &value_D, sizeof(value_D));

        EQ(true, info.waitUpdate());

        checkArrayValues<int32_t>(addh, "TEST/Service_I", value_I[0], value_I[1],
                                  value_I[2], AdditionalDataValue::TypeInt32);
        checkArrayValues<int64_t>(addh, "TEST/Service_X", value_X[0], value_X[1],
                                  value_X[2], AdditionalDataValue::TypeInt64);
        checkArrayValues<int32_t>(addh, "TEST/Service_S", value_S[0], value_S[1],
                                  value_S[2], AdditionalDataValue::TypeInt32);
        checkArrayValues<float>(addh, "TEST/Service_F", value_F[0], value_F[1],
                                value_F[2], AdditionalDataValue::TypeFloat);
        checkArrayValues<double>(addh, "TEST/Service_D", value_D[0], value_D[1],
                                 value_D[2], AdditionalDataValue::TypeDouble);
    }

    void updateMultiArrayValues()
    {
        ADDH addh;
        addh.initConfigurations();
        // don't ask about the const cast ...
        DimInfoWaiter info("TEST/Service_D");

        // Values to be published
        const int32_t value_I[4] = {1, 2, 3, 4};
        longlong value_X[4] = {10, 20, 30, 40};
        short value_S[4] = {11, 12, 13, 14};
        float value_F[4] = {50.50f, 51.50f, 52.50f, 53.50f};
        double value_D[4] = {61.60, 62.60, 63.60, 64.60};

        // Basic Dim Service I
        DimService service_I("TEST/Service_I", "I:2;I:2", &value_I,
                             sizeof(value_I));
        // Basic Dim Service X
        DimService service_X("TEST/Service_X", "X:2;X:2", &value_X,
                             sizeof(value_X));
        // Basic Dim Service S
        DimService service_S("TEST/Service_S", "S:2;S:2", &value_S,
                             sizeof(value_S));
        // Basic Dim Service F
        DimService service_F("TEST/Service_F", "F:2;F:2", &value_F,
                             sizeof(value_F));
        // Basic Dim Service D
        DimService service_D("TEST/Service_D", "D:2;D:2", &value_D,
                             sizeof(value_D));

        EQ(true, info.waitUpdate());

        checkMultyArrayValues<int32_t>(addh, "TEST/Service_I", value_I[0],
                                       value_I[1], value_I[2], value_I[3],
                                       AdditionalDataValue::TypeInt32);
        checkMultyArrayValues<int64_t>(addh, "TEST/Service_X", value_X[0],
                                       value_X[1], value_X[2], value_X[3],
                                       AdditionalDataValue::TypeInt64);
        checkMultyArrayValues<int32_t>(addh, "TEST/Service_S", value_S[0],
                                       value_S[1], value_S[2], value_S[3],
                                       AdditionalDataValue::TypeInt32);
        checkMultyArrayValues<float>(addh, "TEST/Service_F", value_F[0],
                                     value_F[1], value_F[2], value_F[3],
                                     AdditionalDataValue::TypeFloat);
        checkMultyArrayValues<double>(addh, "TEST/Service_D", value_D[0],
                                      value_D[1], value_D[2], value_D[3],
                                      AdditionalDataValue::TypeDouble);
    }

    void updateStringValues()
    {
        ADDH addh;
        addh.initConfigurations();

        DimInfoWaiter info("TEST/Service_C");

        std::string value_C = "this is a string";

        // Basic Dim Service C
        DimService service_C("TEST/Service_C", value_C.c_str());

        EQ(true, info.waitUpdate());

        const ClientsMap list = addh.getClients();
        ClientsMap::const_iterator it = list.find("TEST/Service_C");
        CPPUNIT_ASSERT(it != list.end());
        const ADDHClient &client = it->second;

        const ADDHClientHandler::FieldList values = client.client->getADDHData();
        EQ(std::size_t(1), values.size());

        EQ(AdditionalDataValue::TypeChar, values[0]->type);
        EQ(std::size_t(value_C.size()), values[0]->count());
    }

    void notSupportedStructure()
    {
        ADDH addh;
        addh.initConfigurations();
        DimInfoWaiter info("TEST/Service_I");

        // Values to be published
        const int32_t value_I[3] = {1, 2, 3};
        // Basic Dim Service I (fake multi-format)
        DimService service_I("TEST/Service_I", "I:2;L:1", &value_I,
                             sizeof(value_I));

        EQ(true, info.waitUpdate());

        const ClientsMap list = addh.getClients();
        ClientsMap::const_iterator it = list.find("TEST/Service_I");
        CPPUNIT_ASSERT(it != list.end());
        const ADDHClient &client = it->second;

        const ADDHClientHandler::FieldList values = client.client->getADDHData();
        EQ(std::size_t(0), values.size());
    }

    void notSupportedMultiArrayStructure()
    {
        ADDH addh;
        addh.initConfigurations();
        DimInfoWaiter info("TEST/Service_F");

        // Values to be published
        float value_F[3] = {50.50f, 51.50f, 52.50f};
        // Basic Dim Service I (fake multi-format)
        DimService service_F("TEST/Service_F", "F:1,F:2", &value_F,
                             sizeof(value_F));

        EQ(true, info.waitUpdate());

        const ClientsMap list = addh.getClients();
        ClientsMap::const_iterator it = list.find("TEST/Service_F");
        CPPUNIT_ASSERT(it != list.end());
        const ADDHClient &client = it->second;

        const ADDHClientHandler::FieldList values = client.client->getADDHData();
        EQ(std::size_t(0), values.size());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestADDHBasicDim);

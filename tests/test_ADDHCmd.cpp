#include <fstream>
#include <memory>

#include <boost/filesystem.hpp>

#include <DIMDataSet.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "ADDH.h"
#include "ADDHCmd.h"
#include "Config.h"
#include "WriterFile.h"
#include "local-config.h"
#include "test_helpers.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::addh;

class TestADDHCmdFile : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestADDHCmdFile);
    CPPUNIT_TEST(validatePathCheck);
    CPPUNIT_TEST(receiveCommandWithCorrectAbsolutePath);
    CPPUNIT_TEST(receiveCommandWithCorrectRelativePath);
    CPPUNIT_TEST_SUITE_END();

    bfs::path m_top_level_dir, m_correct_prefix_dir, m_incorrect_prefix_dir;
    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp()
    {
        m_top_level_dir = bfs::temp_directory_path() / "ADDHCmd_test";
        bfs::create_directories(m_top_level_dir);
        m_correct_prefix_dir = m_top_level_dir / "correct_prefix_dir";
        bfs::create_directories(m_correct_prefix_dir);
        m_incorrect_prefix_dir = m_top_level_dir / "incorrect_prefix_dir";
        bfs::create_directories(m_incorrect_prefix_dir);
        m_dim.reset(new DimTestHelper());
    }

    void tearDown()
    {
        bfs::remove_all(m_top_level_dir);
        m_dim.reset();
    }

    void receiveCommand(const bfs::path &filename)
    {
        std::shared_ptr<ADDH> addh(new ADDH());
        WriterFile writer(addh);

        DimInfoWaiter info("ADDH/Events");
        EQ(true, info.waitUpdate());

        writer.addCommand(
            {std::string("write"), 6789, 12345, filename.string()});

        EQ(true, info.waitUpdate(2));

        EQ(true, bfs::exists(filename));
    }

    void receiveCommandWithCorrectRelativePath()
    {
        bfs::path filename = "correct_path";
        receiveCommand(filename);
    }

    void receiveCommandWithCorrectAbsolutePath()
    {
        bfs::path filename = m_correct_prefix_dir / "correct_path";
        receiveCommand(filename);
    }

    void validatePathCheck()
    {
        std::shared_ptr<ADDHCmd> addhCmd_(new ADDHCmd);

        bfs::path correct_prefixed_path = m_correct_prefix_dir / "correct_path";
        bfs::create_directories(correct_prefixed_path);

        bfs::path incorrect_prefixed_path = m_incorrect_prefix_dir /
            "incorrect_path";
        bfs::create_directories(incorrect_prefixed_path);

        bfs::path non_existent_path = m_top_level_dir / "non_existent_path";

        // correct absolute path
        EQ(true,
           addhCmd_->isValidPath(correct_prefixed_path.string(),
                                 m_correct_prefix_dir.string()));

        // correct relative path
        EQ(true,
           addhCmd_->isValidPath("correct_path", m_correct_prefix_dir.string()));

        // incorrect absolute path
        EQ(false,
           addhCmd_->isValidPath(incorrect_prefixed_path.string(),
                                 m_correct_prefix_dir.string()));

        // non existent absolute path
        EQ(false,
           addhCmd_->isValidPath(non_existent_path.string(),
                                 m_correct_prefix_dir.string()));

        // illegal paths
        EQ(false,
           addhCmd_->isValidPath("/tmp/../etc/correct_path",
                                 m_correct_prefix_dir.string()));

        EQ(false,
           addhCmd_->isValidPath(correct_prefixed_path.string() + "/../..",
                                 m_correct_prefix_dir.string()));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestADDHCmdFile);

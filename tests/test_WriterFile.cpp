/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-01-06T14:43:44+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <fstream>
#include <memory>

#include <boost/filesystem.hpp>

#include <DIMDataSet.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "ADDH.h"
#include "Config.h"
#include "WriterFile.h"
#include "local-config.h"
#include "test_helpers.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::addh;

class TestWriterFile : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestWriterFile);
    CPPUNIT_TEST(writeFile);
    CPPUNIT_TEST(writeFileTimingInfo);
    CPPUNIT_TEST(appendFile);
    CPPUNIT_TEST(writeContent);
    CPPUNIT_TEST_SUITE_END();

    bfs::path m_tmp;
    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp()
    {
        m_tmp = bfs::temp_directory_path() / "TestWriterFile";
        bfs::create_directories(m_tmp);

        m_dim.reset(new DimTestHelper());
    }

    void tearDown()
    {
        bfs::remove_all(m_tmp);
        m_dim.reset();
    }

    void writeFile()
    {
        bfs::path filename = m_tmp / "outfile.bin";
        std::shared_ptr<ADDH> addh(new ADDH());
        addh->initConfigurations();
        WriterFile writer(addh);

        ADDHCommand command = {std::string("write"), 6789, 12345,
                               filename.string()};

        DimInfoWaiter info("ADDH/Events");
        EQ(true, info.waitUpdate(1, 2000));

        writer.addCommand(command);

        EQ(true, info.waitUpdate(2));
        CONTAINS(info.getString(), "runNumber=\"12345\"");

        /* empty header file size */
        EQ(uintmax_t(16), bfs::file_size(filename));
    }

    void writeFileTimingInfo()
    {
        bfs::path filename = m_tmp / "outfile.bin";
        std::shared_ptr<ADDH> addh(new ADDH());
        addh->initConfigurations();
        WriterFile writer(addh);

        ADDHCommand command = {
            std::string("write"), 6789,           12345, filename.string(),
            "testLsaCycleValue",  "testUserValue"};

        DimInfoWaiter info("ADDH/Events");
        EQ(true, info.waitUpdate(1, 2000));

        writer.addCommand(command);

        EQ(true, info.waitUpdate(2));
        CONTAINS(info.getString(), "runNumber=\"12345\"");

        /* file size is more than header size */
        EQ(true, uintmax_t(16) < bfs::file_size(filename));
    }

    void appendFile()
    {
        const std::string filename = (m_tmp / "outfile.bin").string();
        const std::string data = "sample data";
        {
            std::ofstream ofs(filename.c_str());
            ofs << data;
            EQ(true, ofs.good());
        }

        std::shared_ptr<ADDH> addh(new ADDH);
        addh->initConfigurations();
        WriterFile writer(addh);

        ADDHCommand command = {std::string("write"), 6789, 12345, filename};

        DimInfoWaiter info("ADDH/Events");
        EQ(true, info.waitUpdate());
        writer.addCommand(command);
        EQ(true, info.waitUpdate(2));

        /* empty header file size */
        EQ(uintmax_t(16 + data.size()), bfs::file_size(filename));
        {
            std::ifstream ifs(filename.c_str());
            std::string buffer(data.size(), '\0');
            ifs.read(&buffer[0], buffer.size());
            EQ(data, buffer);
        }
    }

    void writeContent()
    {
        const std::string filename = (m_tmp / "outfile.bin").string();
        std::shared_ptr<ADDH> addh(new ADDH);
        addh->initConfigurations();
        WriterFile writer(addh);

        DimInfoWaiter infoData("HV_0/OnChannels");
        ntof::dim::DIMDataSet serv("HV_0/OnChannels");
        serv.addData("value", "V", int32_t(42), ntof::dim::AddMode::CREATE);
        EQ(true, infoData.waitUpdate());

        ADDHCommand command = {std::string("write"), 6789, 12345, filename};

        DimInfoWaiter info("ADDH/Events");
        EQ(true, info.waitUpdate());
        writer.addCommand(command);
        EQ(true, info.waitUpdate(2));

        /* ADDH(16) header + content(76) */
        EQ(uintmax_t(16 + 76), bfs::file_size(filename));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestWriterFile);

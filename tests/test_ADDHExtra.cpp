/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-04-14T14:43:44+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include <memory>

#include <boost/filesystem.hpp>

#include <DIMDataSet.h>
#include <cppunit/TestFixture.h>

#include "ADDH.h"
#include "ADDHClientHandler.h"
#include "ADDHExtraService.hpp"
#include "ADDHTypes.hpp"
#include "Config.h"
#include "local-config.h"
#include "test_helpers.hpp"

using namespace ntof::addh;
using namespace ntof::utils;
namespace bfs = boost::filesystem;

class TestADDHExtra : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestADDHExtra);
    CPPUNIT_TEST(loadFromConfig);
    CPPUNIT_TEST(loadFromCommand);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp()
    {
        m_dim.reset(new DimTestHelper());
        bfs::remove(Config::instance().getExtraConfigFile());
        ADDHExtraService::destroy();
    }

    void tearDown()
    {
        ADDHExtraService::destroy();
        m_dim.reset();
    }

    void createConfig(const std::string &fromService,
                      const std::string &fromName,
                      const std::string &destination,
                      pugi::xml_document &doc)
    {
        doc.reset();
        pugi::xml_node extraNode = doc.append_child("extra");
        pugi::xml_node node = extraNode.append_child("data");
        node.append_attribute("fromService").set_value(fromService.c_str());
        node.append_attribute("fromName").set_value(fromName.c_str());
        node.append_attribute("destination").set_value(destination.c_str());
    }

    void loadFromConfig()
    {
        // Create extra conf file with EXTRA/HV_0/OnChannels service
        pugi::xml_document doc;
        createConfig("EXTRA/HV_0/OnChannels", "value", "DEST/EXTRA/HV_0", doc);
        doc.save_file(Config::instance().getExtraConfigFile().c_str());

        ADDH addh;
        addh.initConfigurations();
        DimInfoWaiter info("EXTRA/HV_0/OnChannels");

        EQ(size_t(1), addh.getExtraClients().size());

        ntof::dim::DIMDataSet serv("EXTRA/HV_0/OnChannels");
        serv.addData("value", "V", int32_t(16), ntof::dim::AddMode::CREATE);

        EQ(true, info.waitUpdate());

        const ClientsMap list = addh.getExtraClients();
        EQ(size_t(1), list.size());
        ClientsMap::const_iterator it = list.find("EXTRA/HV_0/OnChannels");
        CPPUNIT_ASSERT(it != list.end());
        const ADDHClient &client = it->second;

        const ADDHClientHandler::FieldList values = client.client->getADDHData();
        EQ(std::size_t(1), values.size());

        EQ(AdditionalDataValue::TypeInt32, values[0]->type);
        EQ(std::size_t(1), values[0]->count());
        EQ(int32_t(16), values[0]->at<int32_t>(0));
    }

    void loadFromCommand()
    {
        // Now extra conf is empty
        ADDH addh;
        addh.initConfigurations();
        DimInfoWaiter info("EXTRA/HV_0/OnChannels");
        DimInfoWaiter extra("ADDH/Extra");

        // Check extra clients is empty
        EQ(size_t(0), addh.getExtraClients().size());

        // Fake extra command
        pugi::xml_document doc;
        createConfig("EXTRA/HV_0/OnChannels", "value", "DEST/EXTRA/HV_0", doc);
        ADDHExtraService::instance().executeCommand(doc.child("extra"));

        ntof::dim::DIMDataSet serv("EXTRA/HV_0/OnChannels");
        serv.addData("value", "V", int32_t(16), ntof::dim::AddMode::CREATE);

        EQ(true, info.waitUpdate());
        EQ(true, extra.waitUpdate());

        // give time to ADDHClientHandler to do his job
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        const ClientsMap list = addh.getExtraClients();
        EQ(size_t(1), list.size());
        ClientsMap::const_iterator it = list.find("EXTRA/HV_0/OnChannels");
        CPPUNIT_ASSERT(it != list.end());
        const ADDHClient &client = it->second;

        const ADDHClientHandler::FieldList values = client.client->getADDHData();
        EQ(std::size_t(1), values.size());

        EQ(AdditionalDataValue::TypeInt32, values[0]->type);
        EQ(std::size_t(1), values[0]->count());
        EQ(int32_t(16), values[0]->at<int32_t>(0));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestADDHExtra);
